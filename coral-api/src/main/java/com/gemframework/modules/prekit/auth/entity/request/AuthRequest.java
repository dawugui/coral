package com.gemframework.modules.prekit.auth.entity.request;

import com.sun.xml.internal.ws.developer.Serialization;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Data
public class AuthRequest implements Serializable {
}
