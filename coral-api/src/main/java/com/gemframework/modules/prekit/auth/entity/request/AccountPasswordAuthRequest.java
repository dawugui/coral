package com.gemframework.modules.prekit.auth.entity.request;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotBlank;

@Slf4j
@Data
public class AccountPasswordAuthRequest extends AuthRequest {

    @NotBlank(message = "account不能为空")
    private String account;
    @NotBlank(message = "password不能为空")
    private String password;
}
