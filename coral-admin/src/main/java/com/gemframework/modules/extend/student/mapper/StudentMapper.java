/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.extend.student.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gemframework.modules.extend.student.entity.Student;
import org.springframework.stereotype.Repository;

/**
 * @Title: StudentMapper
 * @Date: 2020-05-24 23:10:09
 * @Version: v1.0
 * @Description: 学生信息持久层
 * @Author: gem
 * @Email: gemframe@163.com
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Repository
public interface StudentMapper extends BaseMapper<Student> {

    void insert1(String name);

    void insert2(String name);

    void insert3(String name);
}